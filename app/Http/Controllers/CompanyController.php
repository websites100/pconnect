<?php

namespace App\Http\Controllers;

use App\Models\UserTeam;
use App\Models\UserTeamConnect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CompanyController extends Controller
{
    public function showHome()
    {
        $members = UserTeamConnect::getAllTeamMembersPaged(Auth::id());

        return view('company.home',compact('members'));
    }
    public function add_member(Request $request)
    {
        if($id = UserTeam::getOrInsert($request['email'])){
            if(UserTeamConnect::connectTeamUser($id,Auth::id()))
                return back()->with('success',__('Member was successfully added'));
            return back()->with('error',__('User is already a team member'));
        }

        return back()->with('error',__('something has gone wrong'));
    }
    public function remove_member(Request $request)
    {
        if(UserTeamConnect::disconnectTeamUser($request->route('id'),Auth::id()))
            return back()->with('success',__('Member was successfully removed'));

        return back()->with('error',__('something has gone wrong'));
    }
}
