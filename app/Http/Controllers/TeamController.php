<?php

namespace App\Http\Controllers;

use App\Models\UserTeam;
use App\Models\UserTeamConnect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{
    public function showHome()
    {
        $teams = UserTeamConnect::getAllTeamsPaged(Auth::guard('team')->id());

        return view('team.home',compact('teams'));
    }

    public function setName()
    {
        if(Auth::guard('team')->user()->name!="Unknown")
            return redirect()->route('team');

        return view('team.set_name');
    }

    public function saveName(Request $request)
    {
        if(Auth::guard('team')->user()->name=="Unknown"){
            $data = Validator::make($request->all(), [
                'name' => ['required', 'string', 'max:255'],
                'password' => ['required', 'string', 'min:8', 'confirmed'],
            ])->validate();

            UserTeam::changeNameAndPassword($data['name'], $data['password'], Auth::guard('team')->id());
        }

        return redirect()->route('team');
    }
}
