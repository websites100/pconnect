<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TeamLoginController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/team';

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function guard()
    {
        return Auth::guard('team');
    }
    public function showLoginForm()
    {
        return view('auth.team_login');
    }

    public function loggedOut()
    {
        Session::put('locale',App::getLocale());
        Session::save();
    }
}
