<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserTeamConnect extends Model
{

    protected $table = 'user_teams';

    protected $primaryKey = ['user_id', 'user_team_id'];
    public $incrementing = false;

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function user_team()
    {
        return $this->belongsTo(UserTeam::class,'user_team_id','id');
    }

    public static function connectTeamUser($team_user,$user){
        if(self::where('user_id',$user)->where('user_team_id',$team_user)->exists())
            return false;

        $c = new UserTeamConnect;
        $c->user_id = $user;
        $c->user_team_id = $team_user;
        $c->save();
        return true;
    }

    public static function getAllTeamMembersPaged($user,$items=20)
    {
        return self::where('user_id',$user)->paginate($items);
    }

    public static function getAllTeamsPaged($user_team,$items=20)
    {
        return self::where('user_team_id',$user_team)->paginate($items);
    }

    public static function disconnectTeamUser($team_user,$user){
        self::where('user_id',$user)->where('user_team_id',$team_user)->delete();
        return true;
    }
}
