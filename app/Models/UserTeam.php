<?php

namespace App\Models;

use App\Notifications\VerifyTeamEmail;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\HasApiTokens;

class UserTeam extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    protected $table = 'user_team_member';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public static function getOrInsert($email)
    {
        if(!($user = self::where('email', $email)->first())){
            $user = new UserTeam;
            $user->name = 'Unknown';
            $user->email = $email;
            $user->password = Hash::make('nopass');
            $user->save();

            $user->sendEmailVerificationNotification();
        }

        return $user->id;
    }

    public static function changeNameAndPassword($name, $password, $id)
    {
        $user = self::find($id);
        $user->name = $name;
        $user->password = $password;
        $user->save();
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyTeamEmail);
    }
}
