<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\Auth\TeamLoginController;
use App\Http\Controllers\Auth\TeamVerificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::controller(HomeController::class)->group(function (){
    Route::get('/', 'showHome')->name('home');
});

Route::controller(TeamController::class)->prefix('team')->middleware('verified.team')->group(function (){
    Route::middleware('hasName')->group(function (){
        Route::get('/', 'showHome')->name('team');
    });
    Route::get('/set_name', 'setName')->name('team.set_name');
    Route::post('/save_name', 'saveName')->name('team.save_name');
});

Route::controller(CompanyController::class)->prefix('company')->middleware(['auth','verified'])->group(function (){
    Route::get('/', 'showHome')->name('company');

    Route::post('/add_member', 'add_member')->name('company.add_member');
    Route::post('/remove_member/{id}', 'remove_member')->name('company.remove_member');
});

Route::get('change_lang/{lang}', function(Request $request){
    Session::put('locale', $request->lang);
    Session::save();
    return redirect()->back();
})->name('change_lang');

Auth::routes(['verify' => true]);

Route::prefix('team')->group(function (){
    Route::controller(TeamLoginController::class)->group(function (){
        Route::get('login', 'showLoginForm')->name('team.login');
        Route::post('login', 'login')->name('team.login.post');
        Route::post('logout', 'logout')->name('team.logout');
    });

    Route::controller(TeamVerificationController::class)->group(function (){
        Route::post('email/resend', 'resend')->name('team.verification.resend');
        Route::get('email/verify', 'show')->name('team.verification.notice');
        Route::get('email/verify/{id}/{hash}', 'verify')->name('team.verification.verify');
    });
});
