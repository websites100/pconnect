@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12 col-md-4 col-lg-3 order-md-1">
            <h4>{{ __('Add Member') }}</h4>

            <form method="POST" action="{{ route('company.add_member') }}">
                @csrf
                <div class="mb-3">
                    <label for="member_email" class="form-label">{{ __('Email Address') }}</label>
                    <input type="email" class="form-control" name="email" id="member_email">
                </div>
                <button type="submit" class="btn btn-success">{{ __('Add') }}</button>
            </form>
        </div>
        <div class="col-12 col-md-8 col-lg-9 order-md-0">
            <h2 class="text-center">{{ __('My Team Members') }}</h2>
            @if(count($members)>0)
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">{{ __('Name') }}</th>
                        <th scope="col">{{ __('Email Address') }}</th>
                        <th scope="col"></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($members AS $m)
                        <tr>
                            <td>{{ $m->user_team->name }}</td>
                            <td>{{ $m->user_team->email }}</td>
                            <td>
                                <form method="POST" action="{{ route('company.remove_member',[$m->user_team_id]) }}">
                                    @csrf

                                    <button class="btn btn-sm btn-danger" type="submit">
                                        <i class="fa-solid fa-trash-can"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                {!! $members->links() !!}
            @else
                <p class="text-center">{{ __('you currently have no team members') }}</p>
            @endif
        </div>
    </div>
@endsection
