@extends('layouts.app')

@section('content')
    <h2 class="text-center">{{ __('My Teams') }}</h2>
    @if(count($teams)>0)
        <table class="table">
            <thead>
            <tr>
                <th scope="col">{{ __('Name') }}</th>
                <th scope="col">{{ __('Email Address') }}</th>
            </tr>
            </thead>
            <tbody>
            @foreach($teams AS $m)
                <tr>
                    <td>{{ $m->user->name }}</td>
                    <td>{{ $m->user->email }}</td>
                </tr>
            @endforeach
            </tbody>
        </table>

        {!! $teams->links() !!}
    @else
        <p class="text-center">{{ __('you currently have no teams') }}</p>
    @endif
@endsection
